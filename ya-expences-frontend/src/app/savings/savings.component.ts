import { Component, OnInit } from '@angular/core';
import { ApiManagerService } from '../api-manager.service';

@Component({
  selector: 'app-savings',
  templateUrl: './savings.component.html',
  styleUrls: ['./savings.component.css']
})
export class SavingsComponent implements OnInit {

  constructor(private apiManagerService:ApiManagerService) { }
  goodDate = (a:string)=>{
    if(a.length<2){
      return "0"+a;
    }
    return a;
  }
  toMonth = (a:number)=>{
    const months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    return months[a];
  }
  data :any;
  monthAndYear = (x:any) =>{
    const y = String(x)
    return this.toMonth(parseInt(y.slice(5,7)))+" / "+y.slice(0,4);
  }
  ngOnInit(): void {
    this.apiManagerService.findAllSavings().subscribe((d)=>{
      this.data = d;
    })
  }

}
