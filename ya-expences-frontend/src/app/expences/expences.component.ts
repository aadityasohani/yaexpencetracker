import { Component, OnInit } from '@angular/core';
import { ApiManagerService } from '../api-manager.service';

@Component({
  selector: 'app-expences',
  templateUrl: './expences.component.html',
  styleUrls: ['./expences.component.css']
})

export class ExpencesComponent implements OnInit {
  goodDate = (a:string)=>{
    if(a.length<2){
      return "0"+a;
    }
    return a;
  }

  constructor(private apiManagerService:ApiManagerService) { }

  data:any;
  today = new Date();
  d1 = this.today.getFullYear()+"-"+this.goodDate(String(this.today.getMonth()+1))+"-01";
  d2 = this.today.getFullYear()+"-"+this.goodDate(String(this.today.getMonth()+1))+"-"+this.goodDate(String(this.today.getDate()));

  moneySpent = 0;

  forDate1:any;
  forDate2:any;
  getData = () =>{
    this.apiManagerService.getExpences(this.d1,this.d2).subscribe((data)=>{
      this.apiManagerService.expences = data;
      this.data = data;
      this.moneySpent = 0;
      this.data.map((d:any)=>{
        this.moneySpent+=d.spent;
      })
    });
  }

  changeDates = () =>{
    console.log(this.forDate1);
    console.log(this.forDate2);
    this.d1 = this.forDate1.toString();
    this.d2 = this.forDate2.toString();
    this.getData();
  }


  ngOnInit(): void {
    this.getData();
    this.forDate1 = this.d1;
    this.forDate2 = this.d2;
    console.log(this.forDate1,this.d1);
  }

}
